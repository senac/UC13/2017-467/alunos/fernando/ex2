/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.CustoCarro;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class CustoCarroTest {
    
    public CustoCarroTest() {
    }
    
    @Test
    public void deveCalcularCustoFinalConsumidor(){
        CustoCarro custoCarro = new CustoCarro();
        double valorCusto = 10000;
        double resultado = custoCarro.calcular(valorCusto);
        
        assertEquals(17300, resultado , 0.01);
    }
  
    
}
