
package br.com.senac;

public class CustoCarro {
    
    private final double PERCENTAGEM_DISTRIBUIDOR = 0.28;
    private final double IMPOSTOS = 0.45;

    public double calcular(double valorCusto) {
        double imposto = valorCusto * IMPOSTOS;
        double margemLucro = valorCusto * PERCENTAGEM_DISTRIBUIDOR;
        double valorFinal = valorCusto + imposto + margemLucro;
        
        return valorFinal;
    }
}
